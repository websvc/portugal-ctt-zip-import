DROP TABLE IF EXISTS `districts`;
CREATE TABLE IF NOT EXISTS `districts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS `councils`;
CREATE TABLE IF NOT EXISTS `councils` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `district_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_council_district` (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS `localities`;
CREATE TABLE IF NOT EXISTS `localities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `council_id` int NOT NULL,
  `district_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_locality_council` (`council_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

ALTER TABLE `councils` ADD CONSTRAINT `FK_council_district` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`);
ALTER TABLE `localities` ADD CONSTRAINT `FK_locality_council` FOREIGN KEY (`council_id`) REFERENCES `councils` (`id`);
