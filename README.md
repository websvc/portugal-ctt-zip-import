# Portugal CTT ZIP import

This repo holds the CTT provided ZIP codes list and parses the CSV files into MySQL .sql files

## Files information

- todos_cp.zip The file provided by CTT
- todos_cp extracted ZIP
  - concelhos.txt
  - distritos.txt
  - leiame.txt
  - todos_cp.txt
- concelhos.sql MySQL data for concelhos
- freguesias-08.sql MySQL data for parishes (this was gatered from https://pt.wikipedia.org/wiki/Lista_de_freguesias_do_Algarve)
- import.districts.php CSV parser to MySQL for districts
- localidades-08.sql  MySQL data for localities
- structure.sql MySQL structure
- distritos.sql MySQL data for districts
- import.councils.php CSV parser to MySQL for councils
- import.localities.php CSV parser to MySQL for localities
- localidades.sql MySQL data for localities
