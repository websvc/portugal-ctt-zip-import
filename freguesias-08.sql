INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Albufeira e Olhos de Água', '834', '801', '8'),
(NULL, 'Ferreiras', '834', '801', '8'),
(NULL, 'Guia', '834', '801', '8'),
(NULL, 'Paderne', '834', '801', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Alcoutim e Pereiro', '1480', '802', '8'),
(NULL, 'Giões', '1480', '802', '8'),
(NULL, 'Martim Longo', '1480', '802', '8'),
(NULL, 'Vaqueiros', '1480', '802', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Aljezur', '1862', '803', '8'),
(NULL, 'Bordeira', '1862', '803', '8'),
(NULL, 'Odeceixe', '1862', '803', '8'),
(NULL, 'Rogil', '1862', '803', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Altura', '12679', '804', '8'),
(NULL, 'Azinhal', '12679', '804', '8'),
(NULL, 'Castro Marim', '12679', '804', '8'),
(NULL, 'Odeleite', '12679', '804', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Conceição e Estoi', '16492', '805', '8'),
(NULL, 'Faro', '16492', '805', '8'),
(NULL, 'Montenegro', '16492', '805', '8'),
(NULL, 'Santa Bárbara de Nexe', '16492', '805', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Estômbar e Parchal', '20711', '806', '8'),
(NULL, 'Ferragudo', '20711', '806', '8'),
(NULL, 'Lagoa e Carvoeiro', '20711', '806', '8'),
(NULL, 'Porches', '20711', '806', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Bensafrim e Barão de São João', '20818', '807', '8'),
(NULL, 'São Gonçalo de Lagos', '20818', '807', '8'),
(NULL, 'Luz', '20818', '807', '8'),
(NULL, 'Odiáxere', '20818', '807', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Almancil', '21874', '808', '8'),
(NULL, 'Alte', '21874', '808', '8'),
(NULL, 'Ameixial', '21874', '808', '8'),
(NULL, 'Boliqueime', '21874', '808', '8'),
(NULL, 'Quarteira', '21874', '808', '8'),
(NULL, 'Querença, Tôr e Benafim', '21874', '808', '8'),
(NULL, 'Salir', '21874', '808', '8'),
(NULL, 'Loulé (São Clemente)', '21874', '808', '8'),
(NULL, 'Loulé (São Sebastião)', '21874', '808', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Alferce', '28472', '809', '8'),
(NULL, 'Marmelete', '28472', '809', '8'),
(NULL, 'Monchique', '28472', '809', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Moncarapacho e Fuseta', '30937', '810', '8'),
(NULL, 'Olhão', '30937', '810', '8'),
(NULL, 'Pechão', '30937', '810', '8'),
(NULL, 'Quelfes', '30937', '810', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Alvor', '38312', '811', '8'),
(NULL, 'Mexilhoeira Grande', '38312', '811', '8'),
(NULL, 'Portimão', '38312', '811', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'São Brás de Alportel', '43045', '812', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Alcantarilha e Pêra', '44898', '813', '8'),
(NULL, 'Algoz e Tunes', '44898', '813', '8'),
(NULL, 'Armação de Pêra', '44898', '813', '8'),
(NULL, 'São Bartolomeu de Messines', '44898', '813', '8'),
(NULL, 'São Marcos da Serra', '44898', '813', '8'),
(NULL, 'Silves', '44898', '813', '8');

INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Cachopo', '46142', '814', '8'),
(NULL, 'Conceição e Cabanas de Tavira', '46142', '814', '8'),
(NULL, 'Luz de Tavira e Santo Estêvão', '46142', '814', '8'),
(NULL, 'Santa Catarina da Fonte do Bispo', '46142', '814', '8'),
(NULL, 'Santa Luzia', '46142', '814', '8'),
(NULL, 'Tavira', '46142', '814', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Barão de São Miguel', '49871', '815', '8'),
(NULL, 'Budens', '49871', '815', '8'),
(NULL, 'Sagres', '49871', '815', '8'),
(NULL, 'Vila do Bispo e Raposeira', '49871', '815', '8');


INSERT INTO `parishes` (`id`, `name`, `locality_id`, `council_id`, `district_id`)
VALUES
(NULL, 'Monte Gordo', '52416', '816', '8'),
(NULL, 'Vila Nova de Cacela', '52416', '816', '8'),
(NULL, 'Vila Real de Santo António', '52416', '816', '8');
